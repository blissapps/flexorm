## WHAT IS THIS? ##

This is a fork of the RIAForge [FlexORM](http://flexorm.riaforge.org/) project. The source code was taken from the project's last available [Subversion repository](http://svn.riaforge.org/flexorm/).

**FlexORM is an Object Relational Mapping framework for Adobe AIR.**

The built-in SQLite support in Flex for Mobile is, well, barebones. It works, and it works well - but it's quite hostile to RAD, and most apps are hardly going to have complex databases. This is where an ORM comes in, they do all of the boring database stuff, so you can throw objects around in a sort of 'fire and forget' manner.
Adobe don't offer an off-the-shelf solution. This is where FlexORM comes into the game. FlexORM enables JavaScript/ActionScript3 objects to become persistent objects using the embedded SQLite database; that is, you do not need to write database code or SQL. The FlexORM framework can be used by either Flex or JavaScript code in AIR applications.

A key feature of FlexORM is its transparency. You do not need to modify your domain objects or inherit from a base class.

## WHY DID I FORK THIS? ##

I forked the FlexORM sources because the RIAForge project seems to be dead since June 2009 (last public commit).